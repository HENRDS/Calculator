﻿namespace Calculator
    
    [<AutoOpen>]
    module Types =
        [<StructuredFormatDisplay("{Name}")>]
        type MathFunction<'domain, 'range> = 
            {
                Name : string
                ArgCount : int
                Fn : 'domain list -> 'range
            }

        type OperatorTp<'a> =
            | Binary of ('a->'a->'a)
            | Unary of ('a->'a)
        
        type Associativity =
            | Left
            | Right

        [<StructuredFormatDisplay("{Name}")>]
        type Operator<'a> =
            {
                Name : string 
                Assoc : Associativity
                Precedence : int
                Fn : OperatorTp<'a>
            }

        type Token<'a> =
            | Number of 'a
            | Symbol of string
            | Function of MathFunction<'a, 'a>
            | Operator of Operator<'a>
            | LeftParen 
            | RightParen
            | Comma

        type ParserFunctions<'a> = 
            {
                Num : char->bool
                Sym : char->bool
                Oper : char->bool
                fnGetter : string->MathFunction<'a, 'a> option
                opGetter : string->Operator<'a> option
            }

    module GenericMaps = 
        type MapHandler<'key, 'value when 'key : comparison> =
            {
                Adder : 'key->'value->unit
                Getter : 'key->'value option
            }

        let GetMapHandlers<'key, 'value when 'key : comparison> () =
            let d = ref Map.empty<'key, 'value>
            {
             Adder= (fun k v -> d := Map.add k v !d);
             Getter= (fun k -> Map.tryFind k !d)
            }

        open System.Collections.Generic
        let GetDictHandlers<'key, 'value when 'key : comparison> () =
            let d = ref (new Dictionary<'key,'value>())
            {
             Adder= (fun k v -> (!d).[k] <- v);
             Getter= (fun k -> if (!d).ContainsKey(k) then Some (!d).[k] else None)
            }
    #nowarn "64" //The type variable 'a has been constrained to be type 'float'.
    [<AutoOpen>]
    module ReinterpretedFunctions =
        let inline private  invalidArgs<'a> (argc : int) (argv : 'a list)= failwithf "Expected %i arguments, got %i: %A" argc (argv.Length) argv

        let apply1 (f: 'a->'a) (args :'a list) =
            match args with
            | [a] -> f a
            | _ -> invalidArgs 1 args
    
        let apply2 (f: 'a->'a->'a) (args :'a list) =
            match args with
            | [a; b] -> f a b
            | _ -> invalidArgs 2 args
    
        let private rad angle = (angle * System.Math.PI) / 180.0
        let minus = apply1 (~-)
        let sin = apply1 (rad >> System.Math.Sin)
        let cos = apply1 (rad >> System.Math.Cos)
        let tan = apply1 (rad >> System.Math.Tan)
        let sqrt = apply1 System.Math.Sqrt

    //[<AutoOpen>]
    //module Operators =
