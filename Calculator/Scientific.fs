﻿namespace Calculator
    module ScientificCalculator=
        open System

        let NumPattern c = Char.IsDigit(c) || c = '.'
        let SymPattern c = Char.IsLetter(c) || c = '_' || Char.IsDigit(c)
        let OperPattern c =
            let invalidChars = ['_'; '.'; ' '; ','; '('; ')']
            not(Char.IsLetterOrDigit(c) || (invalidChars |> List.contains c))

        open GenericMaps
        let populateFunctions () =
            let x = GetMapHandlers<string, MathFunction<float, float>>()
            x.Adder "-" {Name="-"; ArgCount=1; Fn=minus}
            x.Adder "sin" {Name="sin"; ArgCount=1; Fn=sin}
            x.Adder "cos" {Name="cos"; ArgCount=1; Fn=cos}
            x.Adder "tan" {Name="tan"; ArgCount=1; Fn=tan}
            x.Adder "sqrt" {Name="sqrt"; ArgCount=1; Fn=sqrt}
            x

        let populateOperators () =
            let x = GetMapHandlers<string, Operator<float>>()
            x.Adder "^" {Name="^"; Assoc=Right; Precedence=4; Fn=Binary ( **)}
            x.Adder "*" {Name="*"; Assoc=Left; Precedence=3; Fn=Binary ( *)}
            x.Adder "/" {Name="/"; Assoc=Left; Precedence=3; Fn=Binary (/)}
            x.Adder "+" {Name="+"; Assoc=Left; Precedence=2; Fn=Binary (+)}
            x.Adder "-" {Name="−"; Assoc=Left; Precedence=2; Fn=Binary (-)}
            x 
        let getFunctions() = 
            let ops = populateOperators()
            let funs = populateFunctions()
            {
                Num = NumPattern;
                Sym = SymPattern;
                Oper = OperPattern;
                fnGetter = (funs.Getter);
                opGetter = (ops.Getter); 
            }