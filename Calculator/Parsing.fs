﻿namespace Calculator
    open System
    module Parser =
        let private genericPattern pred value =
            let s, t = value |> List.removeWhile pred
            match s with
            | [] -> None
            | _ -> Some(String.ofCharList s, t)

        let private symPattern pred values =
            let f c = Char.IsLetter(c) || c = '_' || c = '-' 
            match values with
            | c :: _  when f c -> genericPattern pred values
            | _ -> None

        let tokenize functions (line : string) = 
            let (|NUM|_|) = genericPattern (functions.Num)
            let (|SYM|_|) = symPattern (functions.Sym)
            let (|OPER|_|) = genericPattern (functions.Oper)
            let rec _loop acc =
                function
                | [] -> List.rev acc
                | ' ' :: tail -> _loop acc tail
                | '(' :: tail -> _loop (LeftParen :: acc) tail
                | ')' :: tail -> _loop (RightParen:: acc) tail
                | ',' :: tail -> _loop (Comma:: acc) tail
                | NUM (n, tail)-> _loop (Number (n |> Double.Parse) :: acc) tail
                | SYM (s, tail) when  tail.Head = '('-> 
                    match functions.fnGetter s with
                    | Some f -> _loop (Function f :: acc) tail
                    | None -> failwithf "Unknown function name: \'%s\'" s
                | OPER (s, tail) -> 
                    match functions.opGetter s with
                    | Some f -> _loop (Operator f :: acc) tail
                    | None -> failwithf "Unknown operator: \'%s\'" s
                | SYM (s, tail) -> _loop (Symbol s :: acc) tail
                |wtf -> failwithf "Invalid whatever the hell you put here: %A" wtf
            line.ToCharArray() 
            |> List.ofArray 
            |> _loop [] 
    