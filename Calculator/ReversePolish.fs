﻿namespace Calculator
    module ReversePolishNotation =

        let ShuntingYard tokens =
            let isNotLeftParen = function | LeftParen -> false | _ -> true 
            let rec _loop output operators =
                function 
                | [] -> // deal with the parenthesis case
                    (List.rev output) @ operators
                | Comma :: tail -> 
                    let p, rest = operators |> List.removeWhile isNotLeftParen
                    if List.isEmpty rest then failwith "Either the separator was misplaced or parentheses were mismatched."
                    else _loop (p @ output) rest tail
                | Operator o1 :: tail ->
                    let pred = 
                        function 
                        | Operator o2->
                            match o1.Assoc with
                            | Left -> o1.Precedence <= o2.Precedence
                            | Right -> o1.Precedence < o2.Precedence
                        | _ -> false
                    let p, rest = operators |> List.removeWhile pred
                    _loop (p @ output) (Operator o1 :: rest) tail
                | RightParen :: tail ->
                    let o, rest = operators |> List.removeWhile isNotLeftParen
                    if List.isEmpty rest then failwithf "There are mismatched parentheses:\ninput: %A\noutput: %A\noperators: %A" tail output operators
                    else
                        let rest = rest.Tail
                        match rest with
                        | Function f :: t -> _loop (Function f :: (o @ output)) t tail
                        | _ -> _loop (o @ output) rest tail
                | LeftParen :: tail-> _loop output (LeftParen :: operators) tail
                | Function f :: tail -> _loop output (Function f :: operators) tail
                | tk :: tail -> _loop (tk :: output) operators tail
            _loop [] [] tokens

        let private Unstack1 =
            function
            | [] -> failwith "Stack is empty!"
            | a :: t -> (a, t)

        let private Unstack2 =
            function
            | [] -> failwith "Stack is empty!"
            | [x] -> failwithf "There is only 1 element in the stack: %A" x
            | a :: b :: t -> (a, b, t) 

        let Solve<'T> tokens =
            let vars = GenericMaps.GetDictHandlers<string, 'T>()
            do tokens 
            |> List.choose (function | Symbol s -> Some s | _ -> None)
            |> List.distinct
            |> List.iter (fun s -> vars.Adder s (Unchecked.defaultof<'T>))

            let rec _loop (stack : 'T list) =
                function
                | [] -> 
                    if stack.Length = 1 then stack.[0] 
                    else failwithf "Element left in the stack : %A" stack 
                | Number n :: tail -> _loop (n :: stack) tail
                | Symbol s :: tail -> 
                    let v =
                        match vars.Getter s with
                        | Some v -> v
                        | None -> failwithf "Invalid symbol: %s" s
                    _loop ( v :: stack) tail
                | Function f :: tail -> 
                        let args, t = stack |> List.removeN (f.ArgCount)
                        _loop ((args |> f.Fn) :: t) tail
                | Operator o :: tail ->
                    let c, t =
                        match o.Fn with
                        | Binary fn -> 
                            let a, b, t = Unstack2 stack
                            (fn a b, t)
                        | Unary fn ->
                            let a, t = Unstack1 stack
                            (fn a, t)
                    _loop (c :: t) tail
                | nope -> failwithf "Invalid Nope in the stack: %A" nope
            _loop [] tokens


