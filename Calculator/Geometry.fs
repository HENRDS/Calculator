﻿namespace Calculator
    module Geometric =
        open System
        type Vector3D(x : double, y : double, z : double) =
            let norm = x ** 2.0 + y ** 2.0 + z ** 2.0 |> Math.Sqrt
            member this.X = x
            member this.Y = y
            member this.Z = z
            member this.Norm = norm
            member this.Versor = this / norm
            static member (+) (v1 : Vector3D, v2 : Vector3D) = Vector3D(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z)
            static member (-) (v1 : Vector3D, v2 : Vector3D) = Vector3D(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z)
            static member ( *) (v : Vector3D, s : double) = Vector3D(v.X * s, v.Y * s, v.Z * s)
            static member ( *) (s : double, v : Vector3D) = v * s
            static member ( /) (v : Vector3D, s : double) = Vector3D(v.X / s, v.Y / s, v.Z / s)
            static member ( .*) (v1 : Vector3D, v2 : Vector3D) = v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z
            static member op_booleanOr (v1 : Vector3D, v2 : Vector3D) = 
                let b = v1.Y / v2.Y
                (v1.X / v2.X) = b && b = (v1.Z / v2.Z)
            static member op_booleanAnd (v1 : Vector3D, v2 : Vector3D) = v1 .* v2 = 0.0
            static member (^^) (v1 : Vector3D, v2 : Vector3D) = (v1 .* v2) / (v1.Norm * v2.Norm) |> acos
            static member ( **) (v1 : Vector3D, v2 : Vector3D) = 
                let a = v1.Y * v2.Z - v1.Z * v2.Y
                let b = -(v1.X * v2.Z - v1.Z * v2.X)
                let c = v1.X * v2.Y - v1.Y * v2.X
                Vector3D(a, b, c)
            static member (~-) (v : Vector3D) = -1.0 * v
            new (a : double*double*double, b: double*double*double) = 
                let x1, y1, z1 = a
                let x2, y2, z2 = b
                Vector3D(x2-x1, y2-y1, z2-z1) 
            new (v : Vector3D) = Vector3D(v.X, v.Y, v.Z)
            new () = Vector3D(0.0,0.0,0.0)

        module Vector3D =
            let inline unbox (v : Vector3D) = (v.X, v.Y, v.Z)
            let inline toList (v : Vector3D) = [v.X; v.Y; v.Z]
            let proj (v : Vector3D) (u : Vector3D)= (v .* u) / (v.Norm * u.Norm) |> ( *) v

        type Point3D(x : double, y : double, z : double) =
            member this.X = x
            member this.Y = y
            member this.Z = z
            new (v : Point3D) = Point3D(v.X, v.Y, v.Z)
            new () = Point3D(0.0,0.0,0.0)

        module Point3D = 
            let inline unbox (p : Point3D) = (p.X, p.Y, p.Z)
            let inline toList (p : Point3D) = [p.X; p.Y; p.Z]
            let formOrigin (p : Point3D) = p |> unbox |> Vector3D
