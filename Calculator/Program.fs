﻿namespace Calculator
    
    module main =
        open System
        let voidFn x = None

        let translateBack<'T> =
            let fn =
                function 
                | Operator o -> o.Name
                | Function f -> f.Name
                | Number n -> n.ToString()
                | Symbol s -> s
                | LeftParen -> "("
                | RightParen -> ")"
                | Comma -> ","
            (List.map fn >> List.reduce (+))

        open Parser
        open ReversePolishNotation
        open ScientificCalculator

        let doWork fns = tokenize fns >> ShuntingYard

        [<EntryPoint>]
        let main argv = 
            let fns = getFunctions()
            printfn "Populated, parsing.."
            argv 
            |> Array.Parallel.map (doWork fns)
            |> List.ofArray
            |> List.map Solve
            |> List.iter2 (printfn "%s = %f") (argv |> List.ofArray)
            0 // return an integer exit code

