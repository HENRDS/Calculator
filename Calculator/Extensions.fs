﻿namespace Calculator
    open System

    module List =
        let removeWhile<'T> (pred : 'T->bool) (list : 'T list) =
            let rec _loop acc =
                function
                | [] -> (List.rev acc, [])
                | head :: tail when pred head ->_loop (head :: acc) tail
                | head :: tail -> (List.rev acc, head::tail)
            _loop [] list

        let removeN<'a> (count : int) (list : 'a list) =
            let rec _loop n acc (values : 'a list) =
                if n = 0 then (List.rev acc, values)
                else _loop (n - 1) (values.Head :: acc) (values.Tail)
            _loop count [] list

    module Option =
        let ofList<'T> (list : 'T list) =
            match list with
            | [] -> None 
            | x -> Some x

    module String =
        let inline ofCharList (chars : char list) = new string(chars |> Array.ofList)
